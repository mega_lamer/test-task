# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('country', models.CharField(max_length=256, verbose_name=b'Country')),
                ('city', models.CharField(max_length=256, verbose_name=b'City')),
                ('population', models.IntegerField(verbose_name=b'Population')),
            ],
        ),
    ]
