from django.utils.encoding import python_2_unicode_compatible
from django.db import models


@python_2_unicode_compatible
class Item(models.Model):
    country = models.CharField('Country', max_length=256)
    city = models.CharField('City', max_length=256)
    population = models.IntegerField('Population')

    def __str__(self):
        return self.country