from django.utils.encoding import python_2_unicode_compatible
from django.db import models


@python_2_unicode_compatible
class Category(models.Model):
    name = models.CharField('Name', max_length=256)
    description = models.TextField('Description')

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Item(models.Model):
    name = models.CharField('Name', max_length=256)
    categories = models.ManyToManyField(Category, verbose_name='Description')
    number = models.IntegerField('Number')
    price = models.FloatField('Price')

    def __str__(self):
        return self.name