from django.conf.urls import url, include


urlpatterns = [
    url(r'^', include('api.v1.shop.urls')),
    url(r'^stat/', include('api.v1.stat.urls')),
]
