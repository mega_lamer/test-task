from rest_framework import serializers

from shop import models


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Item


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category