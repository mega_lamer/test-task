from django.conf.urls import url

import views

urlpatterns = [
    # Categories API
    url(regex=r'^category/$', view=views.CategoryListView.as_view(), name='category-list'),
    url(regex=r'^category/(?P<pk>[0-9]+)/$', view=views.CategoryDetailView.as_view(), name='category-detail'),

    # Items API
    url(regex=r'^item/$', view=views.ItemListView.as_view(), name='item-list'),
    url(regex=r'^item/(?P<pk>[0-9]+)/$', view=views.ItemDetailView.as_view(), name='item-detail'),
]