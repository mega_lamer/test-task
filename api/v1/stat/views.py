from rest_framework import generics
from rest_framework.permissions import AllowAny


from statistics import models
from . import serializers


class StatViewList(generics.ListAPIView):
    queryset = models.Item.objects.all()
    serializer_class = serializers.StatSerializer
    permission_classes = (AllowAny, )
