from django.conf.urls import url

import views

urlpatterns = [
    url(regex=r'^$', view=views.StatViewList.as_view(), name='stat-list'),
]