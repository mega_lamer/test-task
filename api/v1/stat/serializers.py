from rest_framework import serializers

from statistics import models


class StatSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Item