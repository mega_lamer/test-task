from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import login, logout
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += [
    url(r'^app/api/', include('api.v1.urls', namespace='main')),
    url(r'^app/auth/login/$', login, {'template_name': 'auth/login.html'}, 'login'),
    url(r'^app/auth/logout/$', login_required(logout)),
    url(r'^api/v1/', include('api.v1.urls', namespace='v1')),
]